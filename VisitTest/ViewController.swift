import UIKit
import CoreLocation
import UserNotifications

class ViewController: UIViewController {

  let locationManager = CLLocationManager()
  let dateFormatter: DateFormatter = {
    let formatter = DateFormatter()
    formatter.dateStyle = .short
    formatter.timeStyle = .short
    return formatter
  }()

  override func viewDidLoad() {
    super.viewDidLoad()
    configureLocationServices()
    configureNotifications()
  }
}

// MARK: CLLocationManagerDelegate

extension ViewController: CLLocationManagerDelegate {
  func locationManager(_ manager: CLLocationManager, didVisit visit: CLVisit) {
    var text: String?
    var text2: String?
    if visit.departureDate != Date.distantFuture {
      text = "departed"
      text2 = dateFormatter.string(from: visit.departureDate)
    } else if visit.arrivalDate != Date.distantPast {
      text = "arrived"
      text2 = dateFormatter.string(from: visit.arrivalDate)
    }
    guard let action = text, let date = text2 else { return }
    let coords = "\(visit.coordinate.longitude), \(visit.coordinate.latitude)"
    displayNotification(title: action, subtitle: date, body: coords)
  }
}

// MARK: UNUserNotificationCenterDelegate

extension ViewController: UNUserNotificationCenterDelegate {
  func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
    completionHandler([.sound, .alert, .badge])
  }
}

// MARK: Private 

extension ViewController {
  fileprivate func configureLocationServices() {
    locationManager.delegate = self
    locationManager.allowsBackgroundLocationUpdates = true
    locationManager.pausesLocationUpdatesAutomatically = false
    // Request authorization, if needed.
    let authorizationStatus = CLLocationManager.authorizationStatus()
    switch authorizationStatus {
    case .notDetermined:
      // Request authorization.
      locationManager.requestAlwaysAuthorization()
      break
    default:
      break
    }
    locationManager.startMonitoringVisits()

  }

  fileprivate func configureNotifications() {
    UNUserNotificationCenter.current().delegate = self
    UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) { _, _ in }
  }

  fileprivate func displayNotification(title: String, subtitle: String, body: String) {
    let content = UNMutableNotificationContent()
    content.title = title
    content.subtitle = subtitle
    content.body = body
    let request = UNNotificationRequest(identifier: UUID().uuidString, content: content, trigger: nil)
    UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
  }
}
